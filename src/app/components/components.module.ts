import { ProgramaFormacionComponent } from './programa-formacion/programa-formacion.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';



@NgModule({
  declarations: [
    ProgramaFormacionComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProgramaFormacionComponent
  ]
})
export class ComponentsModule { }
